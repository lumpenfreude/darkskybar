**DarkSkyBar**

Just a simple bash script to scrape weather info from Dark Sky. Intended to be used with something like polybar or py3status, where you can run a script in your i3/BSPWM status bar. 

**Usage:** 

Copy the script to wherever you want to run it from, create a file called darksky.conf in your ~/.config directory and populate it with your Dark Sky API key and latitude/longitude to four decimal points (see example file). Add it to your status bar using whatever script capacity it has. Done. 

**Dependencies:**

* cURL
* Material Design Icons (https://materialdesignicons.com/)
* jq

May work with Font Awesome, but I have no idea. 
